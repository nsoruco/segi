import { ApiController } from './api/api.controller';
import { ApiService } from './api/api.service';
import { HttpService } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ApiModule } from './api/api.module';

@Module({
  imports: [ApiModule],
})
export class AppModule {}
