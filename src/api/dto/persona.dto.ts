import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsBoolean,
  IsDate,
  IsEmail,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Expose, Transform, Type } from 'class-transformer';

export class PersonaReadDto {
  //@Expose({ name: 'carnet_identidad' })
  @Expose()
  carnetIdentidad: string;

  // @Expose({ name: 'complemento' })
  @Expose()
  complemento: string;

  // @Expose({ name: 'paterno' })
  @Expose()
  paterno: string;

  // @Expose({ name: 'materno' })
  @Expose()
  materno: boolean;

  // @Expose({ name: 'nombre' })
  @Expose()
  nombre: string;

  //@Expose({ name: 'fecha_nacimiento' })
  @Expose()
  @Type(() => Date)
  fechaNacimiento: Date;

  //@Expose({ name: 'codigo_rude' })
  // @Expose()
  // codigoRude: string;

  //@Expose({ name: 'genero_tipo_id' })
  //@Expose()
  //generoTipo: GeneroTipoReadDto;

  //@Expose({ name: 'estado_civil_tipo_id' })
  //@Expose()
  //estadoCivilTipo: EstadoCivilTipoReadDto;

  //@Expose({ name: 'sangre_tipo_id' })
  /*@Expose()
  sangreTipo: SangreTipoReadDto;*/

  //@Expose({ name: 'materno_idioma_tipo_id' })
  /*@Expose()
  idiomaTipo: IdiomaTipoReadDto;*/

  //@Expose({ name: 'segip_tipo_id' })
  /*@Expose()
  segipTipo: SegipTipoReadDto;*/

  //@Expose({ name: 'expedido_unidad_territorial_tipo_id' })
  //@Expose()
  //expedidoUnidadTerritorial: UnidadTerritorialReadDto;

  //@Expose({ name: 'nacimiento_unidad_territorial_tipo_id' })
  /*@Expose()
  nacimientoUnidadTerritorial: UnidadTerritorialReadDto;*/

  //@Expose({ name: 'nacimiento_oficialia' })
  // @Expose()
  // nacimientoOficialia: string;

  //@Expose({ name: 'nacimiento_libro' })
  // @Expose()
  // nacimientoLibro: string;

  //@Expose({ name: 'nacimiento_partida' })
  // @Expose()
  // nacimientoPartida: string;

  //@Expose({ name: 'nacimiento_folio' })
  // @Expose()
  // nacimientoFolio: string;

  //@Expose({ name: 'carnet_ibc' })
  // @Expose()
  // carnetIbc: string;

  //@Expose({ name: 'pasaporte' })
  // @Expose()
  // pasaporte: string;

  //@Expose({ name: 'libreta_militar' })
  // @Expose()
  // libretaMilitar: string;

  //@Expose({ name: 'doble_nacionalidad' })
  // @Expose()
  // dobleNacionalidad: boolean;

  //@Expose({ name: 'codigo_rda' })
  // @Expose()
  // codigoRda: string;

  //@Expose({ name: 'nacimiento_localidad' })
  // @Expose()
  // nacimientoLocalidad: string;

  //@Expose({ name: 'observacion' })
  // @Expose()
  // observacion: string;

  //@Expose({ name: 'tiene_discapacidad' })
  // @Expose()
  // tieneDiscapacidad: boolean;

  /*@IsNumber()
  @Type(() => Number)
  //@Expose({ name: 'carnet_identidad' })
  @Expose()
  personaId: number;*/

  // @IsNumber()
  // @Type(() => Number)
  // //@Expose({ name: 'estudiante_id' })
  // @Expose()
  // estudianteId: number;
}

export class PersonaCreateDto {
  carnetIdentidad: string;
  complemento: string;
  paterno: string;
  materno: string;
  nombre: string;
  fechaNacimiento: Date;
  codigoRude: string;
  nacimientoOficialia: string;
  nacimientoLibro: string;
  nacimientoPartida: string;
  nacimientoFolio: string;
  carnetIbc: string;
  pasaporte: string;
  libretaMilitar: string;
  dobleNacionalidad: boolean;
  codigoRda: string;
  nacimientoLocalidad: string;
  observacion: string;
  tieneDiscapacidad: boolean;
  telefono: string;
  email: string;
  generoTipoId: number;
  estadoCivilTipoId: number;
  sangreTipoId: number;
  maternoIdiomaTipoId: number;
  segipTipoId: number;
  expedidoUnidadTerritorialTipoId: number;
  nacimientoUnidadTerritorialTipoId: number;

  usuarioId: number;
  personaId: number;
  estudianteId: number;
}

export class PersonaUpdateDto extends PartialType(PersonaCreateDto) {}

export class PersonaBusquedaCiFechaNacDTO {
  @Expose()
  ci: string;

  @Expose()
  complemento?: string;

  @Expose()
  fechaNac: Date;
}

export class PersonaFilterDto {
  limit: number; // cuento elementos se sacara
  offset: number; // desde que elemento se hara el get
}

export class PersonaSearchDto {
  carnetIdentidad: string; // cuento elementos se sacara
  fechaNacimiento: Date; // desde que elemento se hara el get
  complemento: string;
}
