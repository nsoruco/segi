import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Query,
} from '@nestjs/common';
import { ApiService } from './api.service';
import { PersonaCreateDto, PersonaReadDto } from './dto/persona.dto';

@Controller('api')
export class ApiController {
  constructor(private apiService: ApiService) {}

  @Get('segip/:tipo')
  getCi(
    @Param('tipo', ParseIntPipe) tipo: number,
    @Query() persona: PersonaCreateDto,
  ) {
    return this.apiService.getUrl(tipo, persona);
  }
}
