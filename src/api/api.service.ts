import { HttpService } from '@nestjs/axios';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { create } from 'domain';
import { response } from 'express';
import { type } from 'os';
import { catchError, map, lastValueFrom } from 'rxjs';
import { PersonaCreateDto, PersonaReadDto } from './dto/persona.dto';

@Injectable()
export class ApiService {
  constructor(private http: HttpService) {}

  async getUrl(tipo: number, dto: PersonaCreateDto) {
    const fecha_nac = String(dto.fechaNacimiento);
    const fechaOficial = fecha_nac; //fecha_nac.substring(0, fecha_nac.length - 14);
    const [ano, mes, dia] = fechaOficial.split('-');
    const cambiada = `${dia}/${mes}/${ano}`;
    const complemento = dto.complemento ? dto.complemento : '';
    const paterno = dto.paterno ? dto.paterno : '';
    const lugarNacimientoPais = '';
    const lugarNacimientoDepartamento = '';
    const lugarNacimientoProvincia = '';
    const lugarNacimientoLocalidad = '';

    console.log('fecha: ' + cambiada);
    console.log('complemento: ' + complemento);
    console.log('carnet: ' + dto.carnetIdentidad);
    console.log('nombre: ' + dto.nombre);
    console.log('paterno: ' + paterno);
    console.log(lugarNacimientoPais);
    console.log(lugarNacimientoDepartamento);
    console.log(lugarNacimientoProvincia);
    console.log(lugarNacimientoLocalidad);
    /*const link = `http://100.0.101.79:1336/segip/v2/personas/contrastacion
        ?tipo_persona=${tipo}&lista_campo={"NumeroDocumento":"${dto.carnetIdentidad}",
        "Complemento":"${complemento}",
        "Nombres":"${dto.nombre}",
        "PrimerApellido":"${paterno}",
        "SegundoApellido":"${dto.materno}",
        "FechaNacimiento":"${cambiada}",
        "LugarNacimientoPais":"${lugarNacimientoPais}",
        "LugarNacimientoDepartamento":"${lugarNacimientoDepartamento}",
        "LugarNacimientoProvincia":"${lugarNacimientoProvincia}",
        "LugarNacimientoLocalidad":"${lugarNacimientoLocalidad}"}`;

    /*const valor = await lastValueFrom(
      this.http
        .get(link, { headers: headers })
        .pipe(map((response) => response.data)),
    );*/
    /*const valor = await lastValueFrom(
      this.http.get(link).pipe(map((response) => response.data)),
    );
    const codigoRespuesta = Number(
      valor.ConsultaDatoPersonaContrastacionResult.CodigoRespuesta,
    );
    if (codigoRespuesta === 0) {
      return 'SegipEnum.NO_SE_REALIZO_LA_BUSQUEDA';
    } else if (codigoRespuesta === 1) {
      return 'SegipEnum.NO_SE_ENCONTRO_EL_REGISTRO';
    } else if (codigoRespuesta === 2) {
      return 'SegipEnum.SE_ENCONTRO_1_REGISTRO';
    } else if (codigoRespuesta === 3) {
      return 'SegipEnum.SE_ENCONTRO_MAS_DE_UN_REGISTRO';
    } else if (codigoRespuesta === 4) {
      return 'SegipEnum.REGISTRO_CON_OBSERVACION';
    } else {
      return null;
    }
    /*const tipoPersona = 1;
    const ci = '3978685';
    const link = `http://100.0.101.79:1336/segip/v2/personas/contrastacion?tipo_persona=${tipoPersona}
    &lista_campo={"NumeroDocumento":"${ci}", "Complemento":"", "Nombres":"", "PrimerApellido":"", "SegundoApellido":"", "FechaNacimiento":"17/04/1982", "LugarNacimientoPais":"", "LugarNacimientoDepartamento":"", "LugarNacimientoProvincia":"", "LugarNacimientoLocalidad":""} `;
    const valor = await lastValueFrom(
      this.http.get(link).pipe(map((response) => response.data)),
    );
    const prueba = JSON.parse(
      valor.ConsultaDatoPersonaContrastacionResult.ContrastacionEnFormatoJson,
    );
    valor.ConsultaDatoPersonaContrastacionResult.ContrastacionEnFormatoJson =
      prueba;*/

    //let pru = await (this.consola.create(valor));
    //const segip: SegipMapping = valor;
    //console.log('segip: ', pru);
    //console.log(valor);
    //return valor;
    return 'prueba';
  }
}
